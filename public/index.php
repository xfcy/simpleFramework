<?php

define('BASE_PATH', dirname(__DIR__));
define('SYS_PATH', BASE_PATH . '/system');
define('APP_PATH', BASE_PATH . '/application');

require_once(SYS_PATH . '/AutoLoad.php');

spl_autoload_register(array('AutoLoad', 'load_lib'));
spl_autoload_register(array('AutoLoad', 'load_controller'));

$uri = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
$controller = (empty($tmp = array_shift($uri)) ? 'Index' : $tmp) . 'Controller';
$action = empty($tmp = array_shift($uri)) ? 'index' : $tmp;

try
{
    $obj = new $controller();
    if (method_exists($obj, $action))
    {
        call_user_func_array(array($obj, $action), $uri);
    }
    else
    {
        $error = "Function {$action} in class {$controller} not found.";
        throw new BadFunctionCallException($error);
    }
}
catch (Exception $e)
{
    die($e->getMessage());
}
