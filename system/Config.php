<?php

class Config {

    private static $config;

    public static function load($config_name, $item = false)
    {
        $file_path = APP_PATH . "/config/{$config_name}.php";
        if (!file_exists($file_path))
        {
            return array();
        }

        $result = array();
        if (!isset(static::$config[$config_name]))
        {
            $result = static::$config[$config_name] = require_once($file_path);
        }
        if ($item && isset(static::$config[$config_name][$item]))
        {
            $result = static::$config[$config_name][$item];
        }

        return $result;
    }
}
