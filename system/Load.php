<?php

class Load {

    private static $loaded = array();
    
    public static function helper($name)
    {
        if (empty($name))
        {
            return ;
        }
        if (!is_array($name))
        {
            $name = array($name);
        }
        foreach ($name as $n)
        {
            if (isset(static::$loaded[$n]))
            {
                continue;
            }
            $file_name = BASE_PATH . "/helper/{$n}.php";
            if (file_exists($file_name))
            {
                require_once($file_name);
                static::$loaded[$n] = $n;
            }
        }
    }
}
