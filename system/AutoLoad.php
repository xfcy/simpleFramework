<?php

class AutoLoad {

    public static function load_lib($class_name)
    {
        $file_path = SYS_PATH . '/' . $class_name . '.php';
        if (file_exists($file_path))
        {
            require_once($file_path);
        }
    }
    
    public static function load_controller($class_name)
    {
        $file_path = APP_PATH . '/controller/' . $class_name . '.php';
        if (file_exists($file_path))
        {
            require_once($file_path);
        }
    }
}
