<?php

if (!function_exists('set_cookie'))
{
    function set_cookie(
        string $name, 
        string $value = '', 
        int $expire = 0, 
        string $path = '/', 
        string $domain = '', 
        bool $secure = false, 
        bool $httponly = false
    ) {
        return setcookie($name, $value, time() + $expire, $path, $domain, $secure, $httponly);  
    }
}

if (!function_exists('get_cookie'))
{
    function get_cookie($index)
    {
        if (isset($_COOKIE[$index]))
        {
            return $_COOKIE[$index];
        }
        return false;
    }
}
