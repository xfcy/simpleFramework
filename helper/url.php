<?php

if (!function_exists('base_url'))
{
    function base_url()
    {
        $protocol = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false) ? 'http' : 'https';
        $protocol .= '://';

        $host = $_SERVER['SERVER_NAME'];
        if ($_SERVER['SERVER_PORT'] != 80)
        {
            $host .= ":{$_SERVER['SERVER_PORT']}";
        }

        return $protocol . $host;
    }
}

if (!function_exists('route'))
{
    function route($controller = 'Index', $action = 'index', $params = array())
    {
        $params_str = implode('/', $params);
        return base_url() . "/{$controller}/{$action}/{$params_str}";
    }
}
