<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>
</head>
<body>
    <h1><?=$title?> - <?=$id?></h1>
    <hr>
    <pre><?=var_dump($res)?></pre>
    <hr>
    <a href="<?=$route?>"><?=$route?></a>
</body>
</html>
