<?php

class IndexController extends Controller {
    
    public function index($id = 0)
    {
        Load::helper(array('url', 'cookie'));

        // $db = DB::getInstance('a');
        // var_dump($db);
        $db = DB::getInstance();
        // var_dump($db);
        $sql = "SELECT `USER_NAME` FROM `sys_user`;";
        // $sql = "UPDATE sys_user SET PASS_WORD='b' WHERE USER_ID=1;";
        $result = $db->query($sql);
        $data = array(
            'title' => 'Simple Framework',
            'res' => $result, 
            'id' => $id,
            'route' => route('index', 'index', array(1)),
        );

        // set_cookie('test', json_encode($data, JSON_UNESCAPED_UNICODE), 3600);

        $this->view('index', $data);
    }
}
