<?php

class Controller {

    public function __construct()
    {

    }

    protected function view($file, $data = array())
    {
        $file = str_replace('.', '/', $file) . '.php';
        $view_path = APP_PATH . '/view/' . $file;
        if (file_exists($view_path))
        {
            extract($data);
            require_once($view_path);
        }
        else
        {
            throw new RuntimeException("View template {$file} not found.");
        }
    }

    protected function get_param($name, $default = null)
    {
        if (isset($_GET[$name]) && $_GET[$name] !== '')
        {
            return $_GET[$name];
        }
        elseif (isset($_POST[$name]) && $_POST[$name] !== '')
        {
            return $_POST[$name];
        }

        return $default;
    }
}
